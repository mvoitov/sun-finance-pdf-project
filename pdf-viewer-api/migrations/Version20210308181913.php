<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210308181913 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE preview ADD attachment_id INT DEFAULT NULL, ADD image LONGBLOB NOT NULL');
        $this->addSql('ALTER TABLE preview ADD CONSTRAINT FK_B9852F30464E68B FOREIGN KEY (attachment_id) REFERENCES attachment (id)');
        $this->addSql('CREATE INDEX IDX_B9852F30464E68B ON preview (attachment_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE preview DROP FOREIGN KEY FK_B9852F30464E68B');
        $this->addSql('DROP INDEX IDX_B9852F30464E68B ON preview');
        $this->addSql('ALTER TABLE preview DROP attachment_id, DROP image');
    }
}
