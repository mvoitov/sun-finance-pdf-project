<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210308173706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE attachment ADD previews_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE attachment ADD CONSTRAINT FK_795FD9BBF6BD5216 FOREIGN KEY (previews_id) REFERENCES preview (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_795FD9BBF6BD5216 ON attachment (previews_id)');
        $this->addSql('ALTER TABLE preview DROP INDEX IDX_B9852F30464E68B, ADD UNIQUE INDEX UNIQ_B9852F30464E68B (attachment_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE attachment DROP FOREIGN KEY FK_795FD9BBF6BD5216');
        $this->addSql('DROP INDEX UNIQ_795FD9BBF6BD5216 ON attachment');
        $this->addSql('ALTER TABLE attachment DROP previews_id');
        $this->addSql('ALTER TABLE preview DROP INDEX UNIQ_B9852F30464E68B, ADD INDEX IDX_B9852F30464E68B (attachment_id)');
    }
}
