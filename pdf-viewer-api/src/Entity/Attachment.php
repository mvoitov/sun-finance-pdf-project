<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\AttachmentRepository;
use App\Controller\CreateAttachmentAction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AttachmentRepository::class)
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateAttachmentAction::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "attachment_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *     "get"
 *     },
 * )
 * @Vich\Uploadable
 */
class Attachment
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $added;

    /**
     * @var string|null
     */
    public $contentUrl;

    /**
     * @var File|null
     *
     * @Assert\NotNull(groups={"attachment_create"})
     * @Vich\UploadableField(mapping="attachments", fileNameProperty="filePath")
     */
    public $file;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     */
    private $filePath;

    /**
     * @ORM\OneToMany(targetEntity=Preview::class, mappedBy="attachment")
     * @ApiSubresource()
     */
    private $previews;

    public function __construct()
    {
        $this->previews = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdded(): ?\DateTimeInterface
    {
        return $this->added;
    }

    public function setAdded(\DateTimeInterface $added): self
    {
        $this->added = $added;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * @return Collection|Preview[]
     */
    public function getPreviews(): Collection
    {
        return $this->previews;
    }

    public function addPreview(Preview $preview): self
    {
        if (!$this->previews->contains($preview)) {
            $this->previews[] = $preview;
            $preview->setAttachment($this);
        }

        return $this;
    }

    public function removePreview(Preview $preview): self
    {
        if ($this->previews->removeElement($preview)) {
            // set the owning side to null (unless already changed)
            if ($preview->getAttachment() === $this) {
                $preview->setAttachment(null);
            }
        }

        return $this;
    }
}
