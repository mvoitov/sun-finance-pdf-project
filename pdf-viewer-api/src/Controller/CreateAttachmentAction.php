<?php


namespace App\Controller;


use App\Entity\Attachment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class CreateAttachmentAction
{

    public function __invoke(Request $request): Attachment
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $attachment = new Attachment();
        $attachment->file = $uploadedFile;
        $attachment->setAdded(new \DateTime('now'));
        $attachment->setFilePath('_');

        return $attachment;
    }
}