<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\Attachment;
use App\Entity\Preview;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

final class ResolveAttachmentContentUrlSubscriber implements EventSubscriberInterface
{
    private $storage;
    private $projectDir;
    private $entityManager;

    public function __construct(StorageInterface $storage, KernelInterface $kernel, EntityManagerInterface $entityManager)
    {
        $this->storage = $storage;
        $this->projectDir = $kernel->getProjectDir();
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) || !\is_a($attributes['resource_class'], Attachment::class, true)) {
            return;
        }

        $attachments = $controllerResult;

        if (!is_iterable($attachments)) {
            $attachments = [$attachments];
        }

        foreach ($attachments as $attachment) {
            if (!$attachment instanceof Attachment) {
                continue;
            }

            $attachment->contentUrl = $this->storage->resolveUri($attachment, 'file');
            $pdf = new \Imagick($this->projectDir . '/public/pdf/attachments/' . $attachment->getFilePath());
            $pageCount = $pdf->getnumberimages();
            for ($i = 1; $i < $pageCount; $i++) {
                $pdf = new \Imagick($this->projectDir . '/public/pdf/attachments/' . $attachment->getFilePath() . '[' . $i . ']');
                $preview = new Preview();
                $preview->setAttachment($attachment);
                $pathOfImage = $this->projectDir . '/public/img/attachments/' . uniqid($attachment->getFilePath()) . $i . '.png';
                $pdf->writeImage($pathOfImage);
                $preview->setImage($pathOfImage);
                $this->entityManager->persist($preview);
            }
            $this->entityManager->flush();
        }
    }
}
